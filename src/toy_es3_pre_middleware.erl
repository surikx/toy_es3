%%% @doc
%%% This module implements middleware which should be evaluated before all others 
%%% in the middleware pipeline. Right now, it only stores request `start_time` in `Env`.
%%% `start_time` can be used in the following middlewares, for example see 
%%% `toy_es3_log_middleware.erl`

-module(toy_es3_pre_middleware).

-behaviour(cowboy_middleware).

-export([execute/2]).

execute(Req, Env) -> 
    {ok, Req, maps:put(start_time, erlang:system_time(milli_seconds), Env)}.
