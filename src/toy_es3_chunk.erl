%%% @doc This module works directly with chunks and uses dets as storage.
%%% See toy_es3 module to better understanding how file splits into chunks.

-module(toy_es3_chunk).

% Internal API
-export([init/0, delete_all/0]).

% API to call from toy_es3
-export([write/2,
         read/1,
         delete/1]).

-define(Table, chunk_storage).

%% @private
init() ->
    Opts =[{file, atom_to_list(node()) ++ ".dets"}, 
           {type, set}],
    dets:open_file(?Table, Opts).

%% @private
delete_all() ->
    dets:delete_all_objects(?Table).

-spec write(Key :: any(), Chunk :: binary()) ->
    ok | {error, Reason :: any()}.
write(Key, Chunk) -> 
    dets:insert(?Table, {Key, Chunk}).

-spec read(Key :: any()) ->
    Chunk :: binary() | {error, Reason :: any()}.
read(Key) -> 
    case dets:lookup(?Table, Key) of
        [{Key, Chunk}] -> Chunk;
        _ -> {error, not_found}
    end.

-spec delete(Key :: any()) -> 
    ok | {error, Reason :: any()}.
delete(Key) -> 
    case dets:member(?Table, Key) of
        true ->
            dets:delete(?Table, Key);
        false -> {error, not_found}
    end.
