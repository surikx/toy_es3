%%%-------------------------------------------------------------------
%% @doc toy_es3 public API
%% @end
%%%-------------------------------------------------------------------

-module(toy_es3_app).

-behaviour(application).

%% Application callbacks
-export([start/0, start/2, 
         stop/1]).


%%====================================================================
%% API
%%====================================================================

start() -> application:ensure_all_started(toy_es3).

start(_StartType, _StartArgs) ->
    {ok, Env} = application:get_key(env),

    Port = proplists:get_value(port, Env, 0), % dynamic port
    Nodes = proplists:get_value(nodes, Env, [node()]),

    Dispatch = cowboy_router:compile([
        {'_', [
            {"/v1/file/:name", toy_es3_file_handler, []},
            {'_',              toy_es3_not_found_handler, []}
        ]}
    ]),
    Opts = #{env => #{dispatch => Dispatch},
             middlewares => [toy_es3_pre_middleware, 
                             cowboy_router, 
                             cowboy_handler,
                             toy_es3_log_middleware]},
    {ok, _Pid} = cowboy:start_clear(toy_es3_http, [{port, Port}], Opts),

    error_logger:info_msg("Listening on port: ~p~n", [ranch:get_port(toy_es3_http)]),

    [net_kernel:connect(Node) || Node <- Nodes],

    toy_es3_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
