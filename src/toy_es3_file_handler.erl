-module(toy_es3_file_handler).

-behaviour(cowboy_handler).

-export([init/2]).

-define(OctetStream_CT, #{<<"content-type">> => <<"application/octet-stream">>}).

init(#{method := <<"GET">>} = Req0, State) -> 
    Name = cowboy_req:binding(name, Req0),
    Req = case toy_es3:read(Name) of
              {error, not_found} ->
                  cowboy_req:reply(404, Req0);
              File ->
                  cowboy_req:reply(200, ?OctetStream_CT, File, Req0)
          end,
    {ok, Req, State};

init(#{method := <<"POST">>} = Req0, State) -> 
    Name = cowboy_req:binding(name, Req0),
    {ok, Body, Req1} = cowboy_req:read_body(Req0),
    Req = case toy_es3:write(Name, Body) of
              {error, _Error} ->
                  cowboy_req:reply(500, Req1);
              ok ->
                  cowboy_req:reply(201, Req1)
          end,
    {ok, Req, State};

init(#{method := <<"DELETE">>} = Req0, State) -> 
    Name = cowboy_req:binding(name, Req0),
    Req = case toy_es3:delete(Name) of
              {error, not_found} ->
                  cowboy_req:reply(404, Req0);
              ok ->
                  cowboy_req:reply(200, Req0)
          end,
    {ok, Req, State};

init(Req0, State) -> 
    Req = cowboy_req:reply(405, Req0),
    {ok, Req, State}.
