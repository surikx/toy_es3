-module(toy_es3_log_middleware).

-behaviour(cowboy_middleware).

-export([execute/2]).

execute(#{method := Method, path := Path} = Req, #{start_time := StartTime} = Env) -> 
    Duration = erlang:system_time(milli_seconds) - StartTime, 
    error_logger:info_msg("~s ~s [~p ms]~n", [Method, Path, Duration]),
    {ok, Req, Env};

execute(#{method := Method, path := Path} = Req, Env) -> 
    error_logger:info_msg("~s ~s~n", [Method, Path]),
    {ok, Req, Env}.
