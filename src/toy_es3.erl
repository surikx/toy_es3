%%% @doc This module is basic interface to call toy_es3_chunk via rpc.
%%% For simplification it does rpc call also in single node case.

-module(toy_es3).

-export([write/2,
         read/1,
         delete/1]).

-spec write(Name :: iodata(), Object :: binary()) -> 
    Res :: ok | {error, Reason :: any()}.
write(Name, Object) -> 
    Results = reduce(map(write, [Name, Object])),
    case lists:all(fun(ok) -> true; (_) -> false end, Results) of
        true -> ok;
        false -> {error, Results}
    end.

-spec read(Name :: iodata()) -> 
    Object :: binary() | {error, Reason :: any()}.
read(Name) -> 
    Results = reduce(map(read, [Name])),
    case lists:all(fun(Chunk) when is_binary(Chunk) -> true; (_) -> false end, Results) of
        true ->
            Result = lists:foldl(fun(Data, Acc) -> [Data | Acc] end, [], Results),
            erlang:iolist_to_binary(lists:reverse(Result)); %% TODO: order?
        false -> {error, not_found}
    end.

-spec delete(Name :: iodata()) ->
    Res  :: ok | {error, Reason :: any()}.
delete(Name) -> 
    Results = reduce(map(delete, [Name])),
    case lists:all(fun(ok) -> true; (_) -> false end, Results) of
        true -> ok;
        false -> {error, not_found}
    end.

map(write, [Name, Object] = _Args) ->
    Nodes = get_nodes(),
    Chunks = chunks(Object, length(Nodes)),
    [rpc:async_call(lists:nth(Idx, Nodes), toy_es3_chunk, write, [Name, Chunk]) 
     || {Idx, Chunk} <- Chunks];
map(Command, Args) ->
    Nodes = get_nodes(),
    [rpc:async_call(Node, toy_es3_chunk, Command, Args) || Node <- Nodes].

reduce(Keys) ->
    [rpc:yield(Key) || Key <- Keys].

get_nodes() ->
    Nodes = application:get_env(toy_es3, nodes, [node()]),
    lists:usort(Nodes).

%% helpers to create list of chunks for given binary object
chunks(Object, 0) -> [{1, Object}];
chunks(Object, 1) -> [{1, Object}];
chunks(Object, Size) -> 
    chunks(Object, ceil(size(Object) / Size), []).

chunks(Object, ChunkSize, Acc) when size(Object) > ChunkSize ->
    <<Chunk:ChunkSize/binary, Rest/binary>> = Object,
    chunks(Rest, ChunkSize, [Chunk | Acc]);

chunks(Object, ChunkSize, Acc) when size(Object) =< ChunkSize ->
    Chunks = lists:reverse([Object | Acc]),
    lists:zip(lists:seq(1, length(Chunks)), Chunks).
