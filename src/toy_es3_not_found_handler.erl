-module(toy_es3_not_found_handler).

-behaviour(cowboy_handler).

-export([init/2]).

init(Req0, Env) -> 
    Req = cowboy_req:reply(404, Req0),
    {ok, Req, Env}.
