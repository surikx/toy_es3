toy_es3
=====

Erlang Simple Storage Service – ES3

A toy distributed storage system, the functionality is similar to Amazon S3: 
allow to store, retrieve and delete large data files in distributed fashion.

Build
-----

This is a typical erlang application and can be built with rebar3:

    $ rebar3 compile

Run
-----

Run 3 nodes with predifined configurations:  

    $ rebar3 shell --name a@127.0.0.1 --config test/toy_es3_SUITE_data/a.config --apps toy_es3
    $ rebar3 shell --name b@127.0.0.1 --config test/toy_es3_SUITE_data/b.config --apps toy_es3
    $ rebar3 shell --name c@127.0.0.1 --config test/toy_es3_SUITE_data/c.config --apps toy_es3

Now a@127.0.0.1 can process HTTP requests:

    $ curl -X POST --data-binary @rebar localhost:8080/v1/file/rebar
    $ curl -X GET localhost:8080/v1/file/rebar

Testing
-----

To run dialyzer and common_test run:

    $ make test

Limitations
-----

* No cluster scaling.
* Only works correct when all predifined nodes of cluster are alive. But there are no checks that cluster is ready and in the case of partitioned cluster we can get corrupted data.
* Parallel access to one object is not safe. (read and delete in the same time for example)
* No meta information about objects. `GET /api/files/:name` simply return blob of data.
* The chunking mechanism is dumb. The whole object split to slices equal to cluster size. Each slice stored just once in 'own' node.
* This is not Erlang release but can be easy made with rebar3 and relx.
