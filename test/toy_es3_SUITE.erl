-module(toy_es3_SUITE).

%% Common Test callbacks
-export([all/0,
         suite/0,
         groups/0,
         init_per_suite/1,
         end_per_suite/1,
         group/1,
         init_per_group/2,
         end_per_group/2,
         init_per_testcase/2,
         end_per_testcase/2]).

%% Test cases
-export([not_found_route/0,
         not_found_route/1,
         method_not_allowed/0,
         method_not_allowed/1,
         write_read_delete/0,
         write_read_delete/1,
         read_not_found/0,
         read_not_found/1,
         delete_not_found/0,
         delete_not_found/1]).

-include_lib("common_test/include/ct.hrl").

-define(TestData, <<"This is test data to write in file.">>).

%%%===================================================================
%%% Common Test callbacks
%%%===================================================================

all() ->
    [{group, single_mode},
     {group, distributed_mode}].

all_tests() ->
    [not_found_route,
     method_not_allowed,
     write_read_delete,
     read_not_found,
     delete_not_found].

suite() ->
    [{timetrap, {seconds, 30}}].

groups() ->
    ExcludedCases = [not_found_route, method_not_allowed],
    [{single_mode, [], all_tests()},
     {distributed_mode, [], all_tests() -- ExcludedCases}].

init_per_suite(Config) ->
    {ok, _} = application:ensure_all_started(hackney),
    Config.

end_per_suite(_Config) ->
    ok.

group(_GroupName) ->
    [].

init_per_group(single_mode, Config) ->
    application:load(toy_es3),
    application:set_env(toy_es3, port, 8080),
    {ok, Deps} = application:ensure_all_started(toy_es3),
    [{deps, Deps} | Config];

init_per_group(distributed_mode, Config) ->
    start_slaves(Config),
    Config;

init_per_group(_GroupName, Config) ->
    Deps = config(deps, Config),
    [application:stop(Dep) || Dep <- Deps],
    Config.

end_per_group(single_mode, Config) ->
    toy_es3_chunk:delete_all(),
    Deps = config(deps, Config),
    [application:stop(Dep) || Dep <- Deps],
    Config;

end_per_group(distributed_mode, Config) ->
    stop_slaves(),
    Config;

end_per_group(_GroupName, _Config) ->
    ok.

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, _Config) ->
    ok.

%%%===================================================================
%%% Test cases
%%%===================================================================

not_found_route() ->
    [{doc, "Not Found route works well"}].
not_found_route(_Config) ->
    {ok, 404, <<>>} = request(post, <<"/some_path">>, ?TestData),
    {ok, 404, <<>>} = request(get, <<"/some_path">>),
    {ok, 404, <<>>} = request(delete, <<"/some_path">>),
    ok.

method_not_allowed() ->
    [{doc, "Not allowed method return correct status code"}].
method_not_allowed(_Config) ->
    {ok, 405, <<>>} = request(options, <<"/v1/file/file1">>),
    ok.

write_read_delete() ->
    [{doc, "POST GET and DElETE /v1/file/:name do what expected"}].
write_read_delete(_Config) ->
    {ok, 201, <<>>} = request(post, <<"/v1/file/file1">>, ?TestData),
    {ok, 200, ?TestData} = request(get, <<"/v1/file/file1">>),
    {ok, 200, <<>>} = request(delete, <<"/v1/file/file1">>),
    {ok, 404, <<>>} = request(get, <<"/v1/file/file1">>),
    ok.

read_not_found() ->
    [{doc, "GET /v1/file/:name returns 404 Not Found when file is not available"}].
read_not_found(_Config) ->
    {ok, 404, <<>>} = request(get, <<"/v1/file/not_available_file">>),
    ok.
delete_not_found() ->
    [{doc, "GET /v1/file/:name returns 404 Not Found when file is not available"}].
delete_not_found(_Config) ->
    {ok, 404, <<>>} = request(delete, <<"/v1/file/not_available_file">>),
    ok.

%%%===================================================================
%%% Helpers
%%%===================================================================
config(Key, Config) ->
	{_, Value} = lists:keyfind(Key, 1, Config),
	Value.

request(Method, Path) ->
    request(Method, Path, <<>>).
request(Method, Path, Payload) ->
    Options = [{pool, default}, with_body],
    case hackney:request(Method, <<"localhost:8080", Path/binary>>, [], Payload, Options) of
        {ok, Status, _RespHeaders, Body} ->
            {ok, Status, Body};
        Other -> Other
    end.

-define(Nodes, [{"a", 'a@127.0.0.1'}, 
                {"b", 'b@127.0.0.1'}, 
                {"c", 'c@127.0.0.1'}]).

start_slaves(Config) ->
    start_slaves(Config, ?Nodes).

stop_slaves() ->
    [ct_slave:stop(Node) || {_, Node} <- ?Nodes].

start_slaves(_Config, []) -> ok;
start_slaves(Config, [{Name, Node} | Rest]) ->
    DataDir = config(data_dir, Config),
    ConfigPath = filename:join(DataDir, Name ++ ".config"),
    EbinPath = filename:join(DataDir, "../../../*/ebin"),
    ErlFlags = "-pa " ++ EbinPath ++ " -config " ++ ConfigPath,
    {ok, HostNode} = ct_slave:start(Node,
                                    [{kill_if_fail, true},
                                     {monitor_master, true},
                                     {init_timeout, 3000},
                                     {startup_timeout, 3000},
                                     {startup_functions, [{toy_es3_app, start, []}]},
                                     {erl_flags, ErlFlags}]),
    pong = net_adm:ping(HostNode),
    start_slaves(Config, Rest).
